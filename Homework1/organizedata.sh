#!/bin/bash

##This scrip will place donor files into their own directory which is stores in the fake data directory.

mkdir fakedata;

for donor in 'seq 1 50'
do
    mkdir fakedata/donor${donor}
    mv donor${donor}_tp*.txt fakedata.donor${donor}
done
