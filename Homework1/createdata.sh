#!/bin/bash
## This will create 500 files and within each file will be the header Data 
for x in {1..50}; #the first for loop says that there will be 50 donors 
do 
    for y in {1..10}; #This for loop says that for every donor there will be 10 trials-ie the output is donor1_ tp1...donor1_tp10 
    do 
	echo "Creating file: donor${x}_tp${y}.txt" # prints the creation of every file
	touch donor${x}_tp${y}.txt # Touch creates a file; particularly one with nothing in it
	sed -i '1i Data' donor${x}_tp{y}.txt # Sed inserts the header data into each file
	echo $RANDOM >> donor${x}_tp{y}.txt #
        echo $RANDOM >> donor${x}_tp{y}.txt
	echo $RANDOM >> donor${x}_tp{y}.txt
	echo $RANDOM >> donor${x}_tp{y}.txt
	echo $RANDOM >> donor${x}_tp{y}.txt
	
    done
done


