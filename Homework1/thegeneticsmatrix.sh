#!/bin/bash
## this code will read genetic info in the file hapmap1.ped line by line. 
while read line #while read allows for a file to be read line by line. While read is better at perserving the structure of the text than a for loop.
do  # tells the file that everything following this line is the task that needs to be done
    echo tail "$line" #echo is saying to print each line in the file. Line in a variable ($line) bc while read works by turning each line within a file to a variable. Tail specifies that only the last 10 lines of a file unless otherwise specified
    sleep 1 #this says pause 1 second before outputting the next line
done < hapmap1.ped # Done says the loop is finished and hapmap.1 is the file being read. 
