#!/bin/bash
## This will create 1000 files and within each file will be the header Data 
for x in {001..100}; #the first for loop says that there will be 100 donors 
do 
    for y in {001..010}; #This for loop says that for every donor there will be 10 trials-ie the output is donor001_ tp001...donor001_tp010 
    do 
	echo "Creating file: donor${x}_tp${y}.txt" # prints the creation of every file
	touch donor${x}_tp${y}.txt # Touch creates a file; particularly one with nothing in it
        echo 'Data' >> donor${x}_tp${y}.txt # inserts the header data and random numbers into each file
	echo $RANDOM >> donor${x}_tp${y}.txt 
        echo $RANDOM >> donor${x}_tp${y}.txt
	echo $RANDOM >> donor${x}_tp${y}.txt
	echo $RANDOM >> donor${x}_tp${y}.txt
	echo $RANDOM >> donor${x}_tp${y}.txt
	
    done
done
